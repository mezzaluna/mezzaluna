Italian style located in the middle of Meloneras. You can enjoy lunch and dinner and we will do our 100% best to let you leave with a smile. We have an outside terrace and your also welcome for just a coffee.

Address: C.C. Boulevard Oasis, Calle Mar Mediterráneo, Local 130, 35100 Maspalomas, Las Palmas, Spain

Phone: +34 928 14 68 31
